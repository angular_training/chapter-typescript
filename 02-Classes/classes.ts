// Classes allow us to create 'blueprints' for objects
// In Angular 2 we use classes a lot. For example to create Components, Services, Directives, Pipes, ...

// How to create a class

class Vehicle {
    constructor(private brand) {}

    printVehicleBrand() : void {
        console.log(this.brand);
    }
}

class Car extends Vehicle {
    engineName: string;
    gears: number;
    private speed: number;

    constructor(speed: number, brand: string) {
        super(brand);
        this.speed = speed || 0;
    }

    accelerate(): void {
        this.speed++;
    }

    throttle():void {
        this.speed--;
    }

    getSpeed():void {
        console.log(this.speed);
    }

    static numberOfWheels(): number {
        return 4;
    }
}

// Instantiate (create) an object from a class

let car = new Car(5, 'volvo');
car.accelerate();
car.getSpeed();
car.printVehicleBrand();

console.log(Car.numberOfWheels());