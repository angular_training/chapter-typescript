export const log = val => console.log(val);
export const appender = 'file';

const utilities = {
  log,
  appender
}

export default utilities;