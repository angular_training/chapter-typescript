// var, let & const
// var -> old javascript, still exists in ES6 (function scope)
// let => variable values
// const => constant values, immutable values
console.log('########## LET & CONST ##########');
let trainingTitle = 'Angular 4';
console.log(trainingTitle);

trainingTitle = 'ReactJs';
console.log(trainingTitle);


var testvar = 'blabla';
console.log(testvar);
if(true) {
  var testvar = 'blibli';
}

console.log(testvar);


// arrow functions
console.log('\n########## ARROW FUNCTIONS ##########');
function greet(name) {
  console.log('Hi', name);
}

greet('John Doe');

// why arrow functions
// concise & shorter code [ _ => 7*8 ]
// variety of syntax & implicit return
// return keyword become explicit when using {}
const f = () => 5*7;
const g = () => {
  return 5*8
};
console.log(f());
console.log(g());
// this key word
// ES5 :: n the ES5 example, .bind(this) is required to help pass the this context into the function. 
// Otherwise, by default this would be undefined.
var fobj = {
  id: 42,
  counter: function counter() {
    setTimeout(function() {
      console.log(this.id);
    }.bind(this), 1000);
  }
};
fobj.counter();

// ES6
// ES6 :: arrow functions can’t be bound to a this keyword, so it will lexically go up a scope, 
// and use the value of this in the scope in which it was defined.
var gobj = {
  id: 43,
  counter: function counter() {
    setTimeout(() => {
      console.log(this.id);
    }, 1000);
  }
};
gobj.counter();

// I hope you understand that they do not replace regular functions
// Here are some instances where you probably wouldn’t want to use them:
// Object methods
// When it makes your code less readable
// Callback functions with dynamic context
  // we would get a TypeError. It is because this is not bound to the button, but instead bound to its parent scope.
/* var button = document.getElementById('press');
  button.addEventListener('click', () => {
    this.classList.toggle('on');
  }); */

// no parameter => only use empty ()
// one parameter => () are optional
// two parameters or more => () are mandatory

// arrow functions
console.log('\n########## EXPORTS & IMPORTS ##########');
import person from './person';
import { log as logger } from './utility';
import * as utility from './utility';
import utilities from './utility';
logger(person.name);
utility.log(person.name + ' bis');
utilities.log(person.name);

console.log('\n########## SPREAD & REST OPERATORS ##########');
// SPREAD = Split up array elements or object properties
// Order is important
const numbers = [1, 2, 3];
const newNumbers = [...numbers, 4];
console.log(newNumbers);

let contact = {
  name: 'John Doe',
  email: 'john.doe@mail.com'
}

const fullContact = {
  ...contact,
  phone: '00212666666666'
}
console.log(fullContact);

// REST = Used to merge a list of function arguments into an array
const filter = (...args) => args.filter( el => el >= 3);
console.log(filter(4, 5, 0, 1, 3));

console.log('\n########## DESTRUCTURING ##########');
// Easily extract array elements or object properties and store them in variables
const [a, , b] = [1, 8, 9];
console.log(a, b);

let {email} = contact;
console.log(email);

// Reference & primitive types
// Objects & Arrays are reference type
let nbr1 = 2;
let nbr2 = nbr1;
nbr1 = 5;
console.log(nbr2);

let contact2 = contact; // copy the pointer
contact.name = 'jack sparrow'
console.log('contact2', contact2);

// create a real copy of objects/arrays
let contact3 = { ...contact };
contact.name = 'maximus';
console.log('contact3', contact3);
// Another way to copy object value
let contact4 = (<any>Object).assign({}, contact);
contact.name = 'aaron';
console.log('contact4', contact4);
